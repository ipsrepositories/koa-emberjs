const json = require('koa-json');
const morgan = require('koa-morgan');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const routes = require('./routes');
const config = require('./config');

const app = new Koa();
app.use(morgan('dev'));
app.use(bodyParser());
app.use(json());

app.use(routes.routes()).use(routes.allowedMethods());
app.listen(config.server.port);

console.log(`Server started at http://localhost:${config.server.port}`);
