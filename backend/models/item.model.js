const { sequelize, Sequelize } = require('../db');
const Item = sequelize.define(
  'item',
  {
    name: {
      type: Sequelize.STRING(255),
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at',
    },
  },
  {
    tableName: 'items',
  }
);

module.exports = Item;
