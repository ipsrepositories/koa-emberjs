const hostPort = 3000;
const dbHost = 'localhost';
const dbUser = 'root';
const dbUserPassword = '1234';
const dbName = 'embertutorial';
const dbPort = 9000;

module.exports = {
  server: {
    port: hostPort,
  },
  database: {
    host: dbHost,
    user: dbUser,
    password: dbUserPassword,
    name: dbName,
    port: dbPort,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
};
