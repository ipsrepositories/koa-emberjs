exports.sendOk = (ctx, data) => {
  ctx.status = 200;
  ctx.body = {
    ...data,
  };

  return ctx;
};

exports.serverError = (ctx, message) => {
  ctx.status = 500;
  ctx.body = {
    success: false,
    message,
  };

  return ctx;
};

exports.sendCreated = function (ctx, data) {
  ctx.status = 201;
  ctx.body = {
    result: {
      ...data,
    },
  };
  return ctx;
};

exports.sendOkWithoutContent = function (ctx) {
  return (ctx.status = 204);
};

exports.sendBadRequest = function (ctx, message) {
  ctx.status = 400;
  ctx.body = {
    success: false,
    message,
  };

  return ctx;
};

/**
 * 
 * @param {*} ctx 
 * @param {*} fieldsWithErrors {
                msg: "message",
                param: "field",
                location: "body",
            },
 * @param {*} message 
 * @returns 
 */
exports.sendBadRequestWithFieldErrors = function (
  ctx,
  fieldsWithErrors,
  message
) {
  ctx.status = 400;
  ctx.body = {
    errors: fieldsWithErrors,
    success: false,
    message,
  };

  return ctx;
};

exports.sendUnauthorized = function (ctx, message) {
  ctx.status = 401;
  ctx.body = {
    success: false,
    message,
  };
};

exports.sendForbidden = function (ctx, message) {
  ctx.status = 403;
  ctx.body = {
    success: false,
    message,
  };
};

exports.sendNotFound = function (ctx) {
  ctx.status = 404;
  ctx.body = {
    success: false,
    message: 'Resource not found',
  };
  return ctx;
};

exports.setHeadersForCORS = async function (ctx, next) {
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set(
    'Access-Control-Allow-Headers',
    'Authorization, Origin, X-Requested-With, X-Access-Token, Content-Type, Accept'
  );
  await next();
};
