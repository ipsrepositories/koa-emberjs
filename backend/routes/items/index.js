const Router = require('koa-router');
const router = new Router({
  prefix: '/items',
});

const itemsController = require('../../controllers/items.controller');

router.get('/', itemsController.list).get('/:id', itemsController.read);

module.exports = router;
