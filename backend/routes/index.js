const Router = require('koa-router');
const router = new Router();

const responseHelper = require('../helpers/response');

const itemsRouter = require('./items');

router.use(responseHelper.setHeadersForCORS);
router.use(itemsRouter.routes());

module.exports = router;
