const responseHelper = require('../helpers/response');

const Item = require('../models/item.model');

exports.read = async (ctx, next) => {
  let itemFound = null;
  try {
    itemFound = await Item.findByPk(ctx.params.id);
  } catch (err) {
    // console.log(err.original.code);
    return responseHelper.serverError(ctx, err.original.code);
  }

  if (!itemFound) {
    return responseHelper.sendNotFound(ctx);
  }

  return responseHelper.sendCreated(ctx, itemFound.dataValues);
};

exports.list = async (ctx) => {
  const items = await Item.findAll();

  if (!items) {
    return responseHelper.sendNotFound(ctx);
  }

  return responseHelper.sendCreated(ctx, items);
};
