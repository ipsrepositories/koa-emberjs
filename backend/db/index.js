const config = require('../config');

const Sequelize = require('sequelize');

const sequelize = new Sequelize(
  config.database.name,
  config.database.user,
  config.database.password,
  {
    logging: console.log, // TODO: set console.log only in dev environment
    host: config.database.host,
    dialect: config.database.dialect,
    port: config.database.port,
    timezone: '-05:00',
    pool: {
      max: config.database.pool.max,
      min: config.database.pool.min,
      acquire: config.database.pool.acquire,
      idle: config.database.pool.idle,
    },
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

module.exports = db;
